package com.ritikkanotra.userservice.service;

import com.ritikkanotra.userservice.VO.Department;
import com.ritikkanotra.userservice.VO.ResponseTemplateVO;
import com.ritikkanotra.userservice.entity.User;
import com.ritikkanotra.userservice.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RestTemplate restTemplate;

    public User saveUser(User user) {
        log.info("Inside saveUser method of service layer");
        return userRepository.save(user);
    }

    public User fetchUserById(Long userId) {
        log.info("Inside fetchUserById method of service layer");
        return userRepository.findById(userId).get();
    }

    public ResponseTemplateVO getUserWithDepartment(Long userId) {
        log.info("Inside getUserWithDepartment method of UserService");
        ResponseTemplateVO vo = new ResponseTemplateVO();
        User user = userRepository.findById(userId).get();

        Department department = restTemplate.getForObject("http://DEPARTMENT-SERVICE/departments/" + user.getDepartmentId(), Department.class);
        vo.setUser(user);
        vo.setDepartment(department);

        return vo;

    }
}
