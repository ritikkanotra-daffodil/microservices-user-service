package com.ritikkanotra.userservice.controller;

import com.ritikkanotra.userservice.VO.ResponseTemplateVO;
import com.ritikkanotra.userservice.entity.User;
import com.ritikkanotra.userservice.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/")
    public User saveUser(@RequestBody User user) {
        log.info("Inside saveUser method of controller layer");
        return userService.saveUser(user);
    }

//    @GetMapping("/{id}")
//    public User fetchUserById(@PathVariable("id") Long userId) {
//        log.info("Inside fetchUserById method of controller layer");
//        return userService.fetchUserById(userId);
//    }

    @GetMapping("/{id}")
    public ResponseTemplateVO getUserWithDepartment(@PathVariable("id") Long userId) {
        log.info("Inside getUserWithDepartment method of UserController");
        return userService.getUserWithDepartment(userId);
    }

}
